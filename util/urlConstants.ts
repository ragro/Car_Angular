import {Headers } from "@angular/http";

export class urlConstants{
    private baseUrl = "https://fathomless-savannah-63299.herokuapp.com/";
    
    /**
     * get getAdminUrl
     */return 
    public get getAdminUrl() {
         return this.baseUrl+"admin/";
    }
    public get getUserUrl(){
        return this.baseUrl+"user/";
    }

    public getCarUrl(urlPattern : String){
        if(urlPattern.length > 1)
            return this.baseUrl+"car/"+urlPattern;
        else
            return this.baseUrl + "car/";

    }

    public getHeader(appendToken,appendContentType){
        let headers=new Headers();
        if(appendToken)
        headers.append('Authorization','JWT '+localStorage.getItem('token'));
        if(appendContentType)
        headers.append('Content-Type','application/json');
        return headers;
    }

}